import React, { useState } from "react";
import { Button, Container, Jumbotron } from "react-bootstrap";
import Calendar from "react-calendar";
import { useForm } from "react-hook-form";
import Schedule from "./Forms/Schedule";
import TeacherInfo from "./Forms/TeacherInfo";
import moment from "moment";

const Home = () => {
  const { register, errors, handleSubmit } = useForm();
  const [selectedDays, setSelectedDays] = useState([]);
  const classes = [];
  const [teacher, setTeacher] = useState({});

  const [date, setDate] = useState(new Date());

  const onSubmit = (data) => {
    setTeacher({
      name: data.name,
      subject: data.subject,
      startDate: moment(data.startDate).format("MMMM Do YYYY, dddd"),
      days: selectedDays
        .filter((day) => day.isChecked === true)
        .map((d) => d.value),
    });
    classDays();
  };

  const classDays = () => {
    const startDay = teacher.startDate;
    const nextDateOffset = 0;
    do {
      let nextDate = teacher.startDate;
      nextDate.setDate(startDay.getDate() + nextDateOffset);
      classes.push(nextDate);
    } while (classes.length < 10);
    return classes;
  };

  console.log(teacher);
  console.log(classes);

  const onChange = (date) => {
    console.log(date);
  };

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)}>
        <TeacherInfo
          register={register}
          errors={errors}
          selectedDays={selectedDays}
        />
        <Schedule
          register={register}
          errors={errors}
          selectedDays={selectedDays}
          setSelectedDays={setSelectedDays}
        />
        <Button type="submit">Submit</Button>
      </form>
      <Jumbotron>
        <Calendar activeStartDate={date} onChange={onChange} value={date} />
      </Jumbotron>
    </Container>
  );
};

export default Home;
