import React from "react";
import { Row, Col } from "react-bootstrap";

const Checkbox = ({ id, handleCheck, isChecked, value, register }) => {
  return (
    <li>
      <Row>
        <Col lg="4" className="d-flex align-items-center">
          <input
            ref={register}
            name={value}
            id={id}
            type="checkbox"
            onClick={handleCheck}
            checked={isChecked}
            value={value}
          />
          {value}
        </Col>
        <Col lg="4">
          <label for={`${value}Start`} style={{ paddingRight: "10px" }}>
            Start Time:
          </label>
          <input
            ref={register}
            name={`${value}Start`}
            style={{ width: "200px" }}
            type="Time"
          />
        </Col>
        <Col lg="4">
          <label for={`${value}End`} style={{ paddingRight: "10px" }}>
            End Time:
          </label>
          <input
            ref={register}
            name={`${value}End`}
            style={{ width: "200px" }}
            type="Time"
          />
        </Col>
      </Row>
    </li>
  );
};

export default Checkbox;
