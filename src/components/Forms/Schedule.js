import React, { useState } from "react";
import "./FormStyle.css";
import Checkbox from "./Checkbox";

const Schedule = ({ register, errors, selectedDays, setSelectedDays }) => {
  const [days, setDays] = useState([
    { id: 1, value: "Monday", isChecked: false },
    {
      id: 2,
      value: "Tuesday",
      isChecked: false,
    },
    {
      id: 3,
      value: "Wednesday",
      isChecked: false,
    },
    {
      id: 4,
      value: "Thursday",
      isChecked: false,
    },
    {
      id: 5,
      value: "Friday",
      isChecked: false,
    },
  ]);

  const handleCheck = (event) => {
    let selectedDays = days;
    selectedDays.forEach((day) => {
      if (day.value === event.target.value)
        day.isChecked = event.target.checked;
    });
    setSelectedDays(selectedDays);
  };

  return (
    <div style={{ marginTop: "20px" }}>
      <ul>
        {days.map((day) => {
          return (
            <Checkbox handleCheck={handleCheck} {...day} register={register} />
          );
        })}
      </ul>
    </div>
  );
};

export default Schedule;
