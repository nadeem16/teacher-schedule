import React from "react";
import "./FormStyle.css";
import { Row, Col, Container, Button } from "react-bootstrap";
import moment from "moment";

const TeacherInfo = ({ register, errors, selectedDays }) => {
  return (
    <Container fluid>
      <Row>
        <Col lg="6">
          <input
            style={{ width: "500px", height: "50px" }}
            name="name"
            type="text"
            placeholder="Name"
            ref={register({ required: true })}
          />
          {errors.name && <span>First name is required</span>}
        </Col>
        <Col lg="6">
          <input
            style={{ width: "500px", height: "50px" }}
            name="subject"
            type="text"
            placeholder="Subject"
            ref={register({ required: true })}
          />
          {errors.subject && <span>Subject is Required</span>}
        </Col>
      </Row>
      <hr />
      <Row>
        <Col>
          <label style={{ paddingRight: "10px", fontWeight: "bold" }}>
            Start Date :{" "}
          </label>
          <input
            style={{ width: "500px", height: "50px" }}
            name="startDate"
            type="Date"
            placeholder="Start Date"
            ref={register({ required: true })}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default TeacherInfo;
